// @ts-check
/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-var-requires */

const path = require('path');
const fs = require('fs-extra');
const download = require('download');
const tmp = require('tmp-promise');

const binDir = path.resolve(__dirname, '..', 'assets', 'bin');
const youtubeDlBin = path.resolve(binDir, 'youtube-dl.exe');
const ffmpegBin = path.resolve(binDir, 'ffmpeg.exe');

const ffmpegUrl =
	'https://www.gyan.dev/ffmpeg/builds/ffmpeg-release-essentials.zip';
const youtubeDlUrl = 'https://youtube-dl.org/downloads/latest/youtube-dl.exe';

async function downloadFfmpeg() {
	// FIXME: no stable link to latest ffmpeg build for Windows
	const tmpDir = await tmp.dir();
	console.log(`downloading ffmpeg to temp dir: ${tmpDir.path}`);
	await download(ffmpegUrl, tmpDir.path, { extract: true });

	const files = await fs.readdir(tmpDir.path);
	const ffmpegDir = files[0];
	await fs.copy(path.resolve(tmpDir.path, ffmpegDir, 'bin'), binDir);

	await fs.emptyDir(tmpDir.path);
	await tmpDir.cleanup();

	console.log('downloaded ffmpeg');
	return assertPathExists(ffmpegBin);
}

async function downloadYoutubeDl() {
	console.log('downloading youtube-dl');
	await download(youtubeDlUrl, binDir);

	console.log('downloaded youtubedl');
	return assertPathExists(youtubeDlBin);
}

(async () => {
	// CI_SERVER set by GitLab CI
	if (process.env.CI_SERVER === 'yes') {
		process.stdout.write('Skip running on CI server\n');
		return process.exit(0);
	}

	try {
		await Promise.all([
			assertPathExists(youtubeDlBin),
			assertPathExists(ffmpegBin),
		]);
		// bin exists in ${binDir}, skip download
		return;
	} catch (err) {
		console.warn(`missing bins: ${err.message}`);
	}

	console.log('download missing bins begin');

	console.log(`cleaning bin dir: ${binDir}`);
	await fs.emptyDir(binDir);

	console.log('downloading bin...');
	await Promise.all([downloadFfmpeg(), downloadYoutubeDl()]);

	console.log('checking if bin exists...');
	await Promise.all([
		fs.pathExists(path.resolve(binDir, 'youtube-dl.exe')),
		fs.pathExists(path.resolve(binDir, 'ffmpeg.exe')),
	]);
})()
	.then(() => console.log('download bins completed'))
	.catch((err) => console.error('download bins failed:', err));

async function assertPathExists(somepath) {
	const exists = await fs.pathExists(somepath);
	if (!exists) {
		throw new Error(`file at ${somepath} does not exists`);
	}
}
