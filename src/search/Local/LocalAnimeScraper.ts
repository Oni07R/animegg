import { Anime, AnimeOrigin, AnimeScraper } from '../types';

export class LocalAnimeScraper implements AnimeScraper {
	id: string;
	title: string;
	url: string;
	origin: AnimeOrigin;
	coverUrl?: string;

	private anime: Anime;

	constructor(anime: Anime, origin: AnimeOrigin) {
		this.anime = anime;

		this.id = anime.id;
		this.title = anime.title;
		this.url = anime.url;
		this.coverUrl = anime.coverUrl;
		this.origin = origin;
	}

	async fetchAnime(): Promise<Anime> {
		return {
			...this.anime,
			episodes: this.anime.episodes.map((episode, index) => ({
				...episode,
				index: index + 1,
			})),
		};
	}
}
