import got, { OptionsOfJSONResponseBody } from 'got';
import { CookieJar } from 'tough-cookie';
import userAgent from 'real-user-agent';

interface VvvvidResponseWrapper {
	result: 'ok' | 'error' | string;
	message?: string;
	data?: unknown;
}

interface VvvvidLoginResponse {
	conn_id: string;
}

export default class VvvvidSession {
	private userAgent = '';
	private cookieJar = new CookieJar();
	private connId?: string;

	/**
	 * Effettua il login e ritorna il `conn_id`, spesso usato per fare fetching
	 * di informazioni via le API di VVVVID.
	 * @throws {Error} se non riesce ad ottenere un `conn_id`
	 */
	async login(): Promise<string> {
		if (this.connId) {
			return this.connId;
		}

		if (this.userAgent === '') {
			this.userAgent = await userAgent();
		}

		try {
			const response = await this.requestJson('user/login', {
				method: 'POST',
			});

			if (!response || !(response as VvvvidLoginResponse).conn_id) {
				throw new Error('conn_id non presente');
			}

			this.connId = (response as VvvvidLoginResponse).conn_id;
			return this.connId;
		} catch (err) {
			throw new Error(`Failed fetching conn_id: ${err.message}`);
		}
	}

	/**
	 * Standard request to VVVVID API for JSON response.
	 * @param path relative path to the site vvvvid.it
	 * @throws if any error on the request, the response parsing or an empty response.
	 */
	async get(path: string, requestOpts?: OptionsOfJSONResponseBody) {
		return this.requestJson(path, requestOpts);
	}

	async requestJson(path: string, requestOpts?: OptionsOfJSONResponseBody) {
		// TODO rate-limiting con `p-queue` instead of parallel queue on the higher level?

		// TODO resetSession(): after X time, change user-agent and request new conn_id

		// console.debug('vvvvid requestJson', path);

		const wrapper: VvvvidResponseWrapper = await got(path, {
			...requestOpts,
			prefixUrl: 'https://www.vvvvid.it',
			cookieJar: this.cookieJar,
			// FIXME decompress:true non decomprime...
			// decompress: false,
			headers: {
				/* eslint-disable quote-props */
				Accept: 'application/json',
				Host: 'www.vvvvid.it',
				// 'Origin': 'https://www.vvvvid.it',
				'Accept-Language': 'it-IT,it;q=0.8,en-US;q=0.6,en;q=0.4',
				Connection: 'keep-alive',
				'User-Agent': this.userAgent,
				/* eslint-enable quote-props */
			},
			responseType: 'json',
		}).json();

		// console.log(
		// 	'vvvvid requestJson',
		// 	path,
		// 	'response: ',
		// 	util.inspect(wrapper),
		// );

		if (!wrapper) {
			throw new Error('Empty response?');
		}

		if (!wrapper.data) {
			throw new Error(
				`Empty data. result=${wrapper.result}; message: ${wrapper.message}`,
			);
		}

		return wrapper.data;
	}
}
