import cheerio from 'cheerio';
import AfAnimeScraper from './AfAnimeScraper';
import AfSession from './AfSession';
import { AnimeListScraper, AnimeOrigin } from '../types';

export default class AfListScraper implements AnimeListScraper {
	origin = AnimeOrigin.ANIMEFORCE;

	private session: AfSession;

	constructor(session: AfSession) {
		this.session = session;
	}

	async fetchList(): Promise<AfAnimeScraper[]> {
		const listPage = await this.session.requestRaw(
			'https://ww1.animeforce.org/lista-anime/',
			'http://ay.gy/16031519/www.animeforce.org/lista-anime/',
		);

		const $page = cheerio.load(listPage);

		// Get animes link in animes list
		return $page('#az-slider .item-wrapper a:first-child')
			.toArray()
			.map((link) => {
				const title = $page(link).text();
				const url = $page(link).attr('href');
				if (title && url) {
					return new AfAnimeScraper({
						url: url,
						title: title,
						session: this.session,
					});
				}
			})
			.filter((scraper): scraper is AfAnimeScraper => Boolean(scraper));
	}
}
