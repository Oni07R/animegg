import { AnimeOrigin, AnimeScraper, Episode, EpisodeType } from '../types';

import YtUtils from './YtUtils';
import YtSession from './YtSession';
import YtPlaylistVideosExtractor from './YtPlaylistVideosExtractor';

interface FromListOpts {
	channelName: string;
	channelId: string;
	title: string;
	playlistId: string;
	session?: YtSession;
}

export default class YtPlaylistAnimeScraper implements AnimeScraper {
	id: string;
	url: string;
	title: string;
	origin = AnimeOrigin.YOUTUBE;

	private channelId: string;
	private channelName: string;
	private playlistId: string;

	private session: YtSession;

	constructor(opts: FromListOpts) {
		this.channelId = opts.channelId;
		this.channelName = opts.channelName;
		// TODO pulire titolo anime da "[sub ita...]"
		this.title = opts.title;
		this.id = YtUtils.calculateAnimeId(opts.playlistId);

		this.playlistId = opts.playlistId;
		this.url = YtUtils.playlistUrlFromId(opts.playlistId);
		this.session = opts.session || new YtSession();
	}

	async fetchAnime() {
		const playlistPage = await this.session.requestRaw(this.url);
		const playlistExtractor = new YtPlaylistVideosExtractor(playlistPage);

		const videos = playlistExtractor.extractPlaylistVideos();
		const coverUrl = playlistExtractor.extractPlaylistThumbnailUrl();

		const episodes: Episode[] = videos.map((video, index) => ({
			id: YtUtils.calculateEpisodeId(video.id),
			type: EpisodeType.SEASON,
			animeId: this.id,
			title: video.title,
			index: index + 1,
			url: YtUtils.videoUrlFromId(video.id),
		}));

		return {
			id: this.id,
			title: this.title,
			url: YtUtils.playlistUrlFromId(this.playlistId),
			coverUrl: coverUrl,
			origin: AnimeOrigin.YOUTUBE,
			episodes,
			channelName: this.channelName,
			channelId: this.channelId,
		};
	}
}
