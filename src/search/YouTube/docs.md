# Test per l'estrazione di info da YouTube

## Extract links from page content, via regex

> Ref: class YoutubePlaylistIE, \_VIDEO_RE [ref](https://github.com/rg3/youtube-dl/blob/b5e531f31a57d2cdc9f0edd77e0cbef426e016b9/youtube_dl/extractor/youtube.py#L2078)

Removed named grouping and escaped fwd-slash to make it js compatible.
Added global flag. Moved to non-capturing group id.

```js
const PL_VIDEO_RE = /href="\s*\/watch\?v=([0-9A-Za-z_-]{11})&amp;[^"]*?index=(?:\d+)(?:[^>]+>([^<]+))?/g;
```

## Extract links from page content, via JS

> class YoutubePlaylistBaseInfoExtractor, extract_videos_from_page(self, page) [ref](https://github.com/rg3/youtube-dl/blob/b5e531f31a57d2cdc9f0edd77e0cbef426e016b9/youtube_dl/extractor/youtube.py#L291)

```js
const INITIAL_DATA_RE = /^ *window\["ytInitialData"\] *= *{(.*)};$/m;

function extractVideosByRE(page) {
	return page.match(PL_VIDEO_RE);
}

function extractVideosJSONByRE(page) {
	Paths.cwd.write('testYTPage.html', page);
	const partialJSON = page.match(INITIAL_DATA_RE);
	// match -> [0]: match, [1-N]: capt. grp. 1-N
	const data = JSON.parse(`{${partialJSON[1]}}`);
	// window["ytInitialData"] found on view page source on Firefox
	return data.ytInitialData.contents.twoColumnBrowseResultsRenderer.tabs[0].tabRenderer.content.sectionListRenderer.contents[0].itemSectionRenderer.contents[0].playlistVideoListRenderer.contents
		.map((wrapper) => wrapper.playlistVideoRenderer)
		.filter((video) => video.isPlayable)
		.map((video) => ({
			title: video.title.simpleText,
			url: `${VD_URL_PREFIX}${video.videoId}`,
		}));
}
```
