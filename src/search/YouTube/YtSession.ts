import got from 'got';

export default class YtSession {
	// TODO create got client

	/* eslint-disable class-methods-use-this */
	async requestRaw(url: string) {
		try {
			return (
				await got(url, {
					decompress: false,
					// YouTube sends an already rendered, almost JS-free page if we don't
					// use headers in the request, which can be easily parsed on cheerio
					headers: {
						'User-Agent': '',
					},
				})
			).body;
		} catch (err) {
			throw new Error(`fallito caricamento url ${url}: ${err.message}`);
		}
	}
	/* eslint-enable class-methods-use-this */
}
