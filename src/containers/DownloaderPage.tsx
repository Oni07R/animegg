import React from 'react';
import { observer } from 'mobx-react';

import { useStores } from '../hooks/useStores';
import ScrollableContent from '../components/ScrollableContent';
import { AnimeDownloadSummary } from '../components/AnimeDownloadSummary';

export const DownloaderPage: React.FC = observer(() => {
	const { animedl } = useStores();

	return (
		<div className="flex flex-col w-full h-full bg-primary pt-1 relative z-20">
			{animedl.animesInDownload.length && (
				<ScrollableContent
					className="space-y-3 pb-2 px-3 mr-3 mt-3"
					scrollRestoreKey="downloader"
				>
					{animedl.animesInDownload.map((animeDownload) => (
						<div
							className="bg-white text-black rounded-md shadow-sm py-2 px-3"
							key={animeDownload.animeId}
						>
							<AnimeDownloadSummary animeDownload={animeDownload} />
						</div>
					))}
				</ScrollableContent>
			)}
			{!animedl.animesInDownload.length && (
				<div className="m-auto max-w-xl text-center text-gray-100">
					<p className="text-2xl font-thin">Nessun download aggiunto.</p>
					<p>Aggiungi un anime ai download dalla ricerca.</p>
				</div>
			)}
		</div>
	);
});
