import React from 'react';
import { observer } from 'mobx-react';
import { IoIosWarning } from 'react-icons/io';

import { useStores } from '../hooks/useStores';

import { Notification } from '../components/Notification';
import ScrollableContent from '../components/ScrollableContent';

export const HomePage: React.FC = observer(() => {
	const { animedl } = useStores();

	return (
		<div className="flex px-2 mt-4 max-w-2xl mx-auto">
			<ScrollableContent>
				{animedl.isFetchingAnimeList && (
					<Notification level="info">
						Caricamento della ricerca in corso...
					</Notification>
				)}
				{!animedl.isFetchingAnimeList && animedl.errFetchingAnimeList && (
					<Notification level="warning">
						<span>
							<IoIosWarning className="inline-block mr-2" /> Fallito caricamento
							ricerca.
						</span>
						<br />
						<em>{animedl.errFetchingAnimeList.message}</em>
					</Notification>
				)}
			</ScrollableContent>
		</div>
	);
});
