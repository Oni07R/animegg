/**
 * fnv1a: FNV-1a non-cryptographic hash function
 * @version 1.0.0
 * @see https://github.com/sindresorhus/fnv1a
 */
export function fnv1a(str: string) {
	// Handle Unicode code points > 0x7f
	let hash = 2166136261; // Legacy implementation for 32-bit + number types.
	let isUnicoded = false;

	for (let i = 0; i < str.length; i++) {
		let characterCode = str.charCodeAt(i);

		// Non-ASCII characters trigger the Unicode escape logic
		if (characterCode > 0x7f && !isUnicoded) {
			str = unescape(encodeURIComponent(str));
			characterCode = str.charCodeAt(i);
			isUnicoded = true;
		}

		hash ^= characterCode;
		hash +=
			(hash << 1) + (hash << 4) + (hash << 7) + (hash << 8) + (hash << 24);
	}

	return hash >>> 0;
}
