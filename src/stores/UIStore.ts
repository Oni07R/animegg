import fs from 'fs-extra';
import path from 'path';
import { makeAutoObservable } from 'mobx';
import { remote } from 'electron';

import getSettings, { storeDefaults } from './Settings';

const settings = getSettings();
const win = remote.getCurrentWindow();

export class UIStore {
	showDownloader = false;
	background = storeDefaults.background;

	private backgrounds: string[] = [];

	constructor() {
		makeAutoObservable(this);

		this.loadBackgrounds().then(() => {
			this.restoreSettings();
		});
	}

	toggleDownloader = (toggle = !this.showDownloader) => {
		this.showDownloader = toggle;
	};

	useNextBackground = () => {
		settings.set('background', this.nextBackground);
		this.background = this.nextBackground;
	};

	get nextBackground(): string {
		const index = this.backgrounds.indexOf(this.background);
		let nextBg = this.background;

		if (index < 0 && !this.backgrounds.length) {
			nextBg = this.background;
		} else {
			// Cicla su tutti i background disponibili
			const nextIndex = (index + 1) % this.backgrounds.length;
			nextBg = this.backgrounds[nextIndex];
		}

		return nextBg;
	}

	minimizeWindow() {
		win.minimize();
	}

	maximizeWindow() {
		if (win.isMaximized()) {
			win.unmaximize();
		} else {
			win.maximize();
		}
	}

	closeWindow() {
		// app.quit non sembra far triggherare gli eventi nel renderer, quindi
		// uso window.close perchè li trigghera.
		win.close();
	}

	openDevTools = () => {
		win.webContents.openDevTools();
	};

	private async loadBackgrounds() {
		try {
			const dir = 'assets/backgrounds';
			const filenames = await fs.readdir(dir);
			const filepaths = filenames.map((filename) => path.join(dir, filename));

			this.backgrounds = filepaths;
			console.info('Loaded backgrounds');
		} catch (err) {
			console.error('Failed to load backgrounds', err.message);
		}
	}

	private async restoreSettings() {
		const background = settings.get('background', storeDefaults.background);
		if (await fs.pathExists(background)) {
			this.background = background;
		}
	}
}
