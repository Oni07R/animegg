import { makeAutoObservable } from 'mobx';
import { DownloadStagesCount, DownloadStage } from '../downloader/types';
import path from 'path';
import { remote } from 'electron';
import Fuse from 'fuse.js';

import { AnimeDownload } from './AnimeDownload';
import { EpisodeDownload } from './EpisodeDownload';
import getSettings, { storeDefaults } from './Settings';
import { isDownloadingStage } from '../utils/download';
import { MOCK_SEARCH, ASSETS_DIR_PATH } from '../constants';
import { range, shuffle } from '../utils/array';

import {
	Anime,
	AnimeListScraper,
	AnimeOrigin,
	AnimeScraper,
} from '../search/types';
import { MockAnimeScraper } from '../search/MockAnimeScraper';
import VvvvidListScraper from '../search/Vvvvid/VvvvidListScraper';
import VvvvidSession from '../search/Vvvvid/VvvvidSession';
import AfSession from '../search/Animeforce/AfSession';
import AfListScraper from '../search/Animeforce/AfListScraper';
import YtListScraper, { YtChannel } from '../search/YouTube/YtListScraper';
import YtSession from '../search/YouTube/YtSession';
import LocalListScraper from '../search/Local/LocalListScraper';

/**
 * Limite massimo dei risultati tornati da un AnimeSearchProvider.
 * 60 perchè suddivisibile in n colonne di 5 x 4 x 3.
 */
const SEARCH_RESULTS_LIMIT = 60;

const YOUTUBE_ANIME_CHANNELS: YtChannel[] = [
	{ name: 'Yamato Entertainment', id: 'UCH0Zv73o8yMbH5h44FtehVw' },
];

export type AnimeSearchResult = Readonly<{
	id: string;
	title: string;
	origin: AnimeOrigin;
	coverUrl?: string;
}>;

const settings = getSettings();

export class AnimeDownloaderStore {
	animes = new Map<string, AnimeDownload>();

	queue = new Map<string, EpisodeDownload>();

	downloadDir: string;
	parallelDownloadsLimit: number;
	automatic: boolean;

	listScrapers: Array<{
		name: string;
		listScraper: AnimeListScraper;
		enabled: boolean;
	}>;
	lastSearch = {
		query: '',
		results: [] as AnimeSearchResult[],
	};

	isFetchingAnimeList = false;
	errFetchingAnimeList?: Error;

	constructor() {
		makeAutoObservable(this);

		// Restore settings
		this.downloadDir = settings.get('downloadDir', storeDefaults.downloadDir);
		this.parallelDownloadsLimit = settings.get('parallelsLimit', 1);
		this.automatic = settings.get(
			'automaticDownload',
			storeDefaults.automaticDownload,
		);

		this.listScrapers = [
			{
				name: 'vvvvid',
				listScraper: new VvvvidListScraper(new VvvvidSession()),
				enabled: true,
			},
			{
				name: 'animeforce',
				listScraper: new AfListScraper(new AfSession()),
				enabled: false, // FIXME: update scrape
			},
			{
				name: 'youtube',
				listScraper: new YtListScraper(YOUTUBE_ANIME_CHANNELS, new YtSession()),
				enabled: false, // FIXME: update scrape,
			},
			{
				name: 'animeforce-local',
				listScraper: new LocalListScraper(
					AnimeOrigin.ANIMEFORCE,
					path.resolve(ASSETS_DIR_PATH, 'local-db', 'animeforce.json'),
				),
				enabled: true,
			},
			{
				name: 'youtube-local',
				listScraper: new LocalListScraper(
					AnimeOrigin.YOUTUBE,
					path.resolve(ASSETS_DIR_PATH, 'local-db', 'youtube.json'),
				),
				enabled: true,
			},
		];
	}

	async init(): Promise<void> {
		this.errFetchingAnimeList = undefined;
		this.isFetchingAnimeList = true;
		try {
			await this.fetchAnimeLists();
		} catch (err) {
			this.errFetchingAnimeList = err;
		} finally {
			this.isFetchingAnimeList = false;
		}
	}

	get stagesCount(): DownloadStagesCount {
		const sumStagesCount: DownloadStagesCount = new Map();
		this.animesInDownload.forEach((animeDownload) => {
			animeDownload.stagesCount.forEach((val, stage) => {
				const sum = sumStagesCount.get(stage) || 0;
				sumStagesCount.set(stage, val + sum);
			});
		});
		return sumStagesCount;
	}

	get animesInDownload(): AnimeDownload[] {
		return [...this.animes.values()].filter((anime) => anime.isInDownload);
	}

	addEpisodesToDownload(anime: Anime, episodeIds: string[]) {
		const animeDownload = this.animes.get(anime.id);
		if (animeDownload) {
			const episodesToAdd = episodeIds.filter(
				(episodeId) => !this.queue.has(episodeId),
			);
			episodesToAdd.forEach((episodeId) => {
				const episodeDownload = animeDownload.episodes.find(
					(episode) => episode.id === episodeId,
				);
				if (episodeDownload) {
					this.queue.set(episodeId, episodeDownload);
				}
			});
		}

		this.startNextAutomaticDownload();
	}

	removeEpisodesFromDownload(episodeIds: string[]) {
		episodeIds.forEach((episodeId) => {
			const episodeDownload = this.queue.get(episodeId);
			episodeDownload?.stop();
			this.queue.delete(episodeId);
		});
	}

	addAnimeToDownload(anime: Anime) {
		this.addEpisodesToDownload(
			anime,
			anime.episodes.map((episode) => episode.id),
		);
	}

	removeAnimeFromDownload(anime: Anime) {
		this.removeEpisodesFromDownload(
			anime.episodes.map((episode) => episode.id),
		);
	}

	toggleAutomatic = (toggle = !this.automatic) => {
		this.automatic = toggle;
		this.startNextAutomaticDownload();
		settings.set('automaticDownload', toggle);
	};

	setParallelsLimit(limit: number) {
		this.parallelDownloadsLimit = limit;
		this.startNextAutomaticDownload();
		settings.set('parallelsLimit', limit);
	}

	setDownloadDir(dir: string) {
		this.downloadDir = dir;
		settings.set('downloadDir', dir);
	}

	selectDownloadDir = async () => {
		const opened = await remote.dialog.showOpenDialog({
			title:
				'Seleziona la cartella dove vuoi che vengano salvati i tuoi download',
			defaultPath: this.downloadDir,
			properties: ['openDirectory'],
		});

		if (opened.filePaths && opened.filePaths.length > 0) {
			const downloadDir = opened.filePaths[0];
			this.setDownloadDir(downloadDir);
		}
	};

	onEpisodeDownloadTerminated() {
		this.startNextAutomaticDownload();
	}

	async search(query: string): Promise<void> {
		const animes = [...this.animes.values()];
		let results: AnimeSearchResult[] = [];

		if (query) {
			// TODO Is it costly to re-create a Fuse instance?
			// Should it be updated only when adding/removing animes?
			const fuse = new Fuse(animes, {
				shouldSort: true,
				threshold: 0.6,
				location: 0,
				distance: 100,
				// maxPatternLength: 32,
				minMatchCharLength: 1,
				keys: ['title'],
			});

			results = fuse.search(query).map((result) => result.item);
			console.log(`Search by title "${query}": ${results.length} results`);
		} else {
			results = shuffle(animes);
			console.log('Search random animes');
		}

		results = results.slice(0, SEARCH_RESULTS_LIMIT);

		this.lastSearch = {
			query: query,
			results: results,
		};
	}

	clearLastSearch() {
		this.lastSearch = {
			query: '',
			results: [],
		};
	}

	async fetchAnimeLists() {
		if (MOCK_SEARCH) {
			const animeScrapers = range(30).map(
				(n) => new MockAnimeScraper(`${n}`, { nEpisodes: 10 }),
			);
			animeScrapers.forEach((scraper) => {
				const animeDownload = new AnimeDownload({
					scraper: scraper,
					dir: path.resolve(this.downloadDir, scraper.title),
					store: this,
				});
				this.animes.set(scraper.id, animeDownload);
			});
			console.log(`Loaded mocked animes`, animeScrapers.length);
			return;
		}

		const listScrapers = this.listScrapers
			.filter(({ enabled }) => enabled)
			.map(({ listScraper }) => listScraper);

		await Promise.all(
			listScrapers.map(async (listScraper) => {
				try {
					const scrapers = await listScraper.fetchList();
					this.addAnimeScrapers(scrapers);
					console.log(
						'Completed fetch animes list of',
						listScraper.origin,
						scrapers.length,
					);
				} catch (err) {
					console.warn(
						'Failed to fetch animes list of',
						listScraper.origin,
						err,
					);
				}
			}),
		);
	}

	private addAnimeScrapers(scrapers: AnimeScraper[]): void {
		scrapers.forEach((scraper) => {
			const animeDownload = new AnimeDownload({
				scraper: scraper,
				dir: path.resolve(this.downloadDir, scraper.title),
				store: this,
			});
			this.animes.set(scraper.id, animeDownload);
		});
	}

	private startNextAutomaticDownload = () => {
		if (!this.automatic) {
			return;
		}

		const downloading = [...this.queue.values()].filter((download) =>
			isDownloadingStage(download.stage),
		).length;
		// Mantiene ordine di aggiunta alla queue
		const readyDownloads = [...this.queue.values()].filter(
			(download) => download.stage === DownloadStage.READY,
		);
		const available = this.parallelDownloadsLimit - downloading;

		readyDownloads.slice(0, available).forEach((episode) => {
			episode.start();
		});
	};
}
