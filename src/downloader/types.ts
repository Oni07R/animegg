/** READY -> DOWNLOADING -> FAILED|STOPPED|COMPLETED */
export enum DownloadStage {
	/** Stato iniziale, idle, in queue, pronto al download. */
	READY = 'READY',
	/** In download. */
	DOWNLOADING = 'DOWNLOADING',
	/** Download interrotto volontariamente. */
	STOPPED = 'STOPPED',
	/** Download fallito. Può accadere in stage diversi. */
	FAILED = 'FAILED',
	/** Download completato correttamente. */
	COMPLETED = 'COMPLETED',
}

export type TerminalDownloadStage =
	| DownloadStage.COMPLETED
	| DownloadStage.STOPPED
	| DownloadStage.FAILED;

export interface DownloadState {
	stage: DownloadStage;
	/** Peso totale del download, se possibile ottenerlo. In bytes. */
	sizeTotal?: number;
	/** Peso del download scaricato finora. In bytes. */
	sizeDownloaded: number;
	/** ETA. Tempo rimanente atteso, se possibile ottenerlo. In millisecondi. */
	timeRemaining?: number;
	/** Percentuale del download, se possibile ottenerla. Da 0 a 100. */
	perc?: number;
	/** Breve messaggio; utile per errori o note aggiuntive. */
	message?: string;
}

export type DownloadStagesCount = Map<DownloadStage, number>;

export type DownloadId = string;

// Deve supportare un modello push e pull per lo stage/state del download.
// Push per riportare instantaneamente cambi di stage come COMPLETED o FAILED.
// Pull per aggiornare constantemente sul progresso quando in DOWNLOADING.
export interface Download {
	readonly id: DownloadId;
	readonly filepath: string;

	run(): Promise<TerminalDownloadStage>;
	/** Interrompe il download, se in corso */
	cancel(): Promise<void>;
	/** Ottiene lo stato attuale del download; utile per polling durante il download */
	getState(): Readonly<DownloadState>;
}
