import {
	DownloadStage,
	DownloadState,
	Download,
	DownloadId,
	TerminalDownloadStage,
} from './types';
import onetime from 'onetime';
import { isTerminalStage } from '../utils/download';

interface Opts {
	timeToComplete: number;
	// TODO fail after X time
}

const defaultOpts: Opts = {
	timeToComplete: 10 * 1000, // 10 sec
};

export class FakeDownload implements Download {
	readonly id: string;
	readonly filepath: string;
	private state: DownloadState & { sizeTotal: number } = {
		sizeDownloaded: 0,
		sizeTotal: 200 * 1000 * 1000, // 200 MB
		timeRemaining: 0,
		stage: DownloadStage.DOWNLOADING,
		perc: 0,
	};
	private downloadResolve?: (value?: TerminalDownloadStage) => void;
	private downloading?: {
		startTime: number;
		completedTimeout: NodeJS.Timeout;
	};
	private opts: Opts;

	constructor(id: DownloadId, opts: Opts = defaultOpts) {
		this.id = id;
		this.filepath = `/fake/path/download/${id}`;
		this.state.timeRemaining = opts.timeToComplete;
		this.opts = opts;
	}

	async run(): Promise<TerminalDownloadStage> {
		const download = new Promise<TerminalDownloadStage>((resolve) => {
			this.downloadResolve = resolve;
		});

		this.downloading = {
			startTime: Date.now(),
			completedTimeout: global.setTimeout(() => {
				this.state.perc = 100;
				this.state.timeRemaining = 0;
				this.state.sizeDownloaded = this.state.sizeTotal;
				this.stopAtStage(DownloadStage.COMPLETED);
			}, this.opts.timeToComplete),
		};

		return download;
	}

	async cancel(): Promise<void> {
		this.stopAtStage(DownloadStage.STOPPED);
	}

	getState(): DownloadState {
		if (isTerminalStage(this.state.stage)) {
			return { ...this.state };
		}

		const { timeToComplete } = this.opts;
		const { sizeTotal } = this.state;
		const timeElapsed = Date.now() - (this.downloading?.startTime || 0);
		const timeRemaining = Math.max(timeToComplete - timeElapsed, 0);
		const sizeDownloaded = Math.min(
			sizeTotal - (timeRemaining / timeToComplete) * sizeTotal,
			sizeTotal,
		);

		this.state.sizeDownloaded = sizeDownloaded;
		this.state.timeRemaining = timeRemaining;
		this.state.perc = Math.min((sizeDownloaded / sizeTotal) * 100, 100);
		return { ...this.state };
	}

	private stopAtStage = onetime((stage: TerminalDownloadStage) => {
		this.state.stage = stage;
		if (this.downloading) {
			global.clearTimeout(this.downloading.completedTimeout);
		}
		this.downloadResolve?.(stage);
	});
}
