import { DownloadState } from '../../downloader/types';

// Esempio output download youtube-dl
// "[download]  21.9% of ~350.41MiB at  2.17MiB/s ETA 01:43"
// Al completamento
// "[download] 100% of 7.73MiB"
// => ['download']
const SECTION_RE = /\[(\w+)\]/;
// => ['21.9']
const PERC_RE = /([0-9.]+)%/;
// => ['350.41', 'MiB'|'KiB']
const SIZE_TOTAL_RE = /of\s*~*([0-9.]+)(\w+)/;
// => ['2.17', 'MiB'|'KiB']
// const SPEED_RE = /at\s*~*([0-9\.]+)([\w]+)/;
// => ['01:43']
// const TIME_REMAINING_RE = /ETA\s*~*([0-9\:]+)/;

/**
 * Interpreta la stringa di ouput del processo `youtube-dl` per aggiornare
 * lo stato di avanzamento del download.
 */
export default function ytdlNativeHlsOutput(
	prevState: DownloadState,
	output: string,
): DownloadState {
	if (output === '') {
		return prevState;
	}

	// FIXME le linee cominciano con `/r`, e forse contengono altri caratteri
	// speciali per il terminale che sarebbe meglio rimuovere

	const matchSection = output.match(SECTION_RE);
	if (!matchSection) {
		// Se non trova una sezione potrebbe non riuscire a parsare il resto
		// dell'output
		return prevState;
	}

	const [, section] = matchSection;
	if (section !== 'download') {
		// Non è la sezione "download", non troverà dati utili per lo state
		return prevState;
	}

	const state = { ...prevState };

	const matchSizeTotal = output.match(SIZE_TOTAL_RE);
	if (matchSizeTotal) {
		const [, sizeTotal, sizeUnit] = matchSizeTotal;
		const sizeTotalNum = Number.parseInt(sizeTotal, 10);
		// TODO leggere sizeUnit, che può essere 'MiB', 'GiB', ecc
		// Assumo che sia sembre in MiB, transformo in bytes
		const sizeTotalKb = sizeTotalNum * 1000 * 1000;
		state.sizeTotal = sizeTotalKb;
	}

	const matchPerc = output.match(PERC_RE);
	if (matchPerc) {
		const [, perc] = matchPerc;
		// => num in range [0.0, 100.0]
		const percNum = Number.parseFloat(perc);
		state.perc = percNum;

		// perc : 100 = xRemainingSize : sizeTotal
		const sizeTotal = state.sizeTotal || 0;
		const sizeDownloaded = (percNum * sizeTotal) / 100;
		state.sizeDownloaded = sizeDownloaded;
	}

	// TODO utilizzare ETA?
	// TODO utilizzare velocità download?

	return state;
}
