import React from 'react';

import { shell } from 'electron';
import validator from 'validator';

export default function ExternalLink(
	props: React.ComponentPropsWithoutRef<'a'>,
) {
	const onClick = () => {
		const { href } = props;
		if (href && validator.isURL(href)) {
			shell.openExternal(href);
		}
	};

	return <a {...props} onClick={onClick} />;
}
