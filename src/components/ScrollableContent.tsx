import React from 'react';
import classNames from 'classnames';

// No ref perchè è già utilizzata internamente per salvare lo scroll
type Props = React.ComponentPropsWithoutRef<'div'> & {
	scrollRestoreKey?: string;
};

/**
 * Cache/map delle posizioni dello scroll per una determinata key.
 * Simile a `react-scroll-manager` ma fatto in casa.
 */
const scrollPositions: { [key: string]: number } = {};

export default class ScrollableContent extends React.Component<Props> {
	private scrollRef = React.createRef<HTMLDivElement>();

	componentDidMount() {
		const { scrollRestoreKey } = this.props;
		const scrollable = this.scrollRef.current;

		if (scrollable && scrollRestoreKey) {
			scrollable.scrollTop = scrollPositions[scrollRestoreKey] || 0;
		}
	}

	componentWillUnmount() {
		const { scrollRestoreKey } = this.props;
		const scrollable = this.scrollRef.current;

		if (scrollable && scrollRestoreKey) {
			scrollPositions[scrollRestoreKey] = scrollable.scrollTop || 0;
		}
	}

	render() {
		return (
			<div
				// FIXME React warning che sto passando props non di DOM a div
				// perchè ho `scrollRestoreKey` in più
				// {...this.props}
				className={classNames(
					'scrollbar-primary flex-grow overflow-y-auto max-h-full pr-3',
					this.props.className,
				)}
				ref={this.scrollRef}
			>
				{this.props.children}
			</div>
		);
	}
}
