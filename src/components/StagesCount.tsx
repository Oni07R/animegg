import React from 'react';

import { DownloadStagesCount, DownloadStage } from '../downloader/types';

const stagesColor: { [x: string]: string } = {
	[DownloadStage.COMPLETED]: 'tag-success',
	[DownloadStage.FAILED]: 'tag-warning',
};

export const StageCount = (props: { stage: DownloadStage; count: number }) => (
	<span className={`tag ${stagesColor[props.stage] || ''}`}>
		{props.stage}: {props.count}
	</span>
);

export const StagesCount = (props: { stagesCount: DownloadStagesCount }) => (
	<div className="flex space-x-1">
		{[...props.stagesCount.entries()].map(([stage, count]) => (
			<StageCount key={stage} stage={stage} count={count} />
		))}
	</div>
);
