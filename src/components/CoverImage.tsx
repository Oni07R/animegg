import React from 'react';

const defaultCover = 'assets/images/404.jpg';

type Props = React.ComponentPropsWithoutRef<'img'>;

interface State {
	src: string;
}

export default class CoverImage extends React.Component<Props, State> {
	constructor(props: Props) {
		super(props);

		const src = !props.src ? defaultCover : props.src;

		this.state = {
			src,
		};
	}

	handleLoadingError = () => {
		if (this.state.src !== defaultCover) {
			this.setState({ src: defaultCover });
		}
	};

	render() {
		const { src } = this.state;

		return (
			<img
				{...this.props}
				src={src}
				// `alt` va lasciato vuoto, sennò si vede l'icona dell'immagine mancante
				alt=""
				decoding="async"
				onError={this.handleLoadingError}
				style={{ background: '#ccc' }}
			/>
		);
	}
}
