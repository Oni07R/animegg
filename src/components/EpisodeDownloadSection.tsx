import { observer } from 'mobx-react';
import React from 'react';
import { DownloadStage } from '../downloader/types';

import { EpisodeDownload } from '../stores/EpisodeDownload';

interface Props {
	title: string;
	episodes: EpisodeDownload[];
	onAddAll: () => void;
	onRemoveAll: () => void;
}

export const EpisodeDownloadSection: React.FC<Props> = observer((props) => {
	const { title, episodes, onAddAll, onRemoveAll, children } = props;
	const totalEpisodes = episodes.length;
	const inDownloads = episodes.filter(
		(ep) => ep.stage === DownloadStage.DOWNLOADING,
	).length;

	const hasAvailable = totalEpisodes - inDownloads > 0;
	const hasDownloading = inDownloads > 0;

	return (
		<div className="pb-8">
			<div
				className="flex items-center pb-2"
				style={{
					background: 'linear-gradient(to bottom, white, 95%, transparent)',
				}}
			>
				<div className="flex-grow">
					<p className="text-xs text-gray-500 -mb-1">
						GRUPPO — {totalEpisodes} Episodi
					</p>
					<p className="text-3xl leading-tight">{title}</p>
				</div>
				<div className="space-x-2">
					<button
						className="button"
						title="Ferma e rimuovi gli tutti gli episodi in download di questa lista"
						onClick={onRemoveAll}
						disabled={!hasDownloading}
					>
						Rimuovi tutti
					</button>
					<button
						className="button button-primary"
						title="Scarica tutti gli episodi non ancora in download di questa lista"
						onClick={onAddAll}
						disabled={!hasAvailable}
					>
						Scarica tutti
					</button>
				</div>
			</div>
			<div>{children}</div>
		</div>
	);
});
