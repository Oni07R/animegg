import React from 'react';
import classnames from 'classnames';
import { IoIosClose } from 'react-icons/io';

interface Props {
	show: boolean;
	onHide: () => void;
}

export const Modal: React.FC<Props> = ({ show, children, onHide }) => {
	// TODO aggiugnere animazione apertura/chiusura zoom in/out
	return (
		<div
			className={classnames(
				'fixed inset-0 items-center justify-center flex flex-col overflow-hidden z-40',
				!show && 'hidden',
			)}
		>
			<div
				className="absolute inset-0"
				style={{ backgroundColor: 'rgba(0,0,0,0.4)' }}
				onClick={onHide}
			/>
			<div className="relative overflow-auto my-12 mx-auto w-2/3 rounded-lg bg-white p-6 no-draggable">
				<button
					className="absolute right-0 top-0 p-4 text-4xl text-gray-600"
					onClick={onHide}
				>
					<IoIosClose />
				</button>
				{children}
			</div>
		</div>
	);
};
