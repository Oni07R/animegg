import React from 'react';

import { DownloadStage } from '../downloader/types';
import ProgressBar from './ProgressBar';

const MAX_PERC = 100;

interface Props {
	stage: DownloadStage;
	perc?: number;
	message?: string;
}

const DownloadStateProgress: React.FC<Props> = (props) => {
	const { stage, message } = props;

	let perc = props.perc || 0;

	if (stage === DownloadStage.COMPLETED) {
		perc = MAX_PERC;
	}

	return (
		<div>
			<ProgressBar value={perc} max={MAX_PERC} showPercentage={true} />
			<p className="text-xs text-gray-500 font-bold">
				{stage}
				{message ? ` — ${message}` : ''}
			</p>
		</div>
	);
};

export default DownloadStateProgress;
