import React from 'react';
import classNames from 'classnames';

interface Props {
	value: number;
	max?: number;
	showPercentage?: boolean;
}

export default function ProgressBar(props: Props) {
	const { value } = props;
	const max = props.max || 100;

	const isCompleted = value >= max;

	return (
		<div className="flex justify-between items-center">
			<progress
				className={classNames(
					'progress flex-grow',
					isCompleted && 'progress-success',
				)}
				value={value}
				max={max}
			/>
			{props.showPercentage && (
				<small className="ml-2 w-10 text-xs">
					{value >= max ? 100 : Math.ceil((value / max) * 100)}%
				</small>
			)}
		</div>
	);
}
