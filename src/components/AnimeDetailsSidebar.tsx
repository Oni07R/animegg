import React from 'react';
import { FiExternalLink, FiYoutube } from 'react-icons/fi';

import { Anime } from '../search/types';

import ExternalLink from './ExternalLink';
import CoverImage from './CoverImage';
import { ANIME_ORIGIN_INFO } from '../search/AnimeOriginInfo';

interface Props {
	anime: Anime;
	children?: React.ReactNode;
	className?: string;
}

const AnimeDetailsSidebar: React.FC<Props> = ({
	anime,
	className,
	children,
}) => {
	return (
		<div className={className}>
			<div className="relative h-64 border-solid border-1 border-gray-200">
				<ExternalLink href={anime.coverUrl}>
					<CoverImage
						className="object-cover object-top h-full w-full"
						src={anime.coverUrl}
					/>
				</ExternalLink>
				<div className="absolute inset-x-0 bottom-0 px-4 py-3 bg-white-overlay">
					<span className="tag float-right">
						{ANIME_ORIGIN_INFO[anime.origin].name}
					</span>
					<p>Episodi: {anime.episodes.length}</p>
					<p>
						<ExtraDetails anime={anime} />
					</p>
				</div>
			</div>
			<div className="px-4 py-3">
				<ExternalLink href={anime.url}>
					Apri su {anime.origin}&nbsp;
					<FiExternalLink className="inline" />
				</ExternalLink>
			</div>
			<hr />
			<div className="px-4 py-3">
				<ExternalLink
					href={`https://www.google.it/search?q=${encodeURI(anime.title)}`}
				>
					Cerca informazioni&nbsp;
					<FiExternalLink className="inline" />
				</ExternalLink>
			</div>
			{children && (
				<>
					<hr />
					<div className="px-4 py-3">{children}</div>
				</>
			)}
		</div>
	);
};

export default AnimeDetailsSidebar;

const ExtraDetails: React.FC<{ anime: Anime }> = (props) => {
	const { anime } = props;

	if (anime.channelName && anime.channelId) {
		return (
			<ExternalLink
				title="Apri il canale su YouTube"
				href={`https://www.youtube.com/channel/${anime.channelId}`}
			>
				{anime.channelName}&nbsp;
				<FiYoutube className="inline" />
			</ExternalLink>
		);
	}

	if (anime.subberName || anime.subberUrl) {
		return (
			<ExternalLink
				href={anime.subberUrl || anime.url}
				title="Apri il sito ufficiale del subber/dubber dell'anime"
			>
				{anime.subberName || '(Subber/Dubber)'}&nbsp;
				<FiExternalLink className="inline" />
			</ExternalLink>
		);
	}

	return null;
};
