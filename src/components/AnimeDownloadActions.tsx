import { observer } from 'mobx-react';
import React from 'react';
import { IoIosFolderOpen } from 'react-icons/io';

import { useStores } from '../hooks/useStores';

interface Props {
	animeId: string;
}

export const AnimeDownloadActions: React.FC<Props> = observer(({ animeId }) => {
	const { animedl } = useStores();
	const animeDownload = animedl.animes.get(animeId);

	if (!animeDownload) {
		return null;
	}

	return (
		<button
			className="button is-primary is-outlined is-fullwidth"
			title="Apri la cartella nel computer con gli episodi già scaricati"
			onClick={animeDownload.openDownloadDir}
		>
			Apri Anime&nbsp;
			<IoIosFolderOpen />
		</button>
	);
});
