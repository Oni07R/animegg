// @ts-check
const path = require('path');
const { merge } = require('webpack-merge');
const webpack = require('webpack');

const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');

module.exports = (env, options) => {
	const isHot = options.hot === true;
	const isDevelopment = options.mode !== 'production';

	/**
	 * Base config
	 * @type {webpack.Configuration}
	 */
	const baseConfig = {
		output: {
			path: path.resolve(__dirname, './dist'),
			// https://github.com/webpack/webpack/issues/1114
			libraryTarget: 'commonjs2',
		},
		resolve: {
			// For TypeScript file support
			extensions: ['.tsx', '.ts', '.js', '.json'],
		},
		// Disables webpack processing of __dirname and __filename.
		// https://github.com/webpack/webpack/issues/2010
		node: {
			__dirname: false,
			__filename: false,
		},
		stats: {
			// Silence warnings for `got` and its linked libraries
			// https://github.com/webpack/webpack/issues/8826
			moduleTrace: false,
			modules: false,
		},
	};

	/**
	 * Webpack config for `electron-main`.
	 */
	const main = merge(baseConfig, {
		target: 'electron-main',
		entry: './src/main.ts',
		output: {
			filename: 'main.js',
		},

		module: {
			rules: [
				{
					test: /\.(ts)|(js)$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader',
						options: {
							cacheDirectory: true, // use cache for faster rebuilds
							babelrc: false, // don't use .babelrc if present
							presets: [
								['@babel/env', { targets: { electron: '10' } }],
								'@babel/typescript',
							],
							plugins: [
								['@babel/plugin-proposal-class-properties', { loose: true }],
								'@babel/plugin-proposal-optional-chaining',
							],
						},
					},
				},
			],
		},
	});

	/**
	 * Webpack config for `electron-renderer`.
	 * @see https://github.com/Automattic/simplenote-electron/blob/develop/webpack.config.js
	 */
	const renderer = merge(baseConfig, {
		target: 'electron-renderer',
		entry: './src/renderer.tsx',
		output: {
			filename: 'renderer.js',
		},

		// Useful for debugging and testing with TypeScript
		devtool: 'source-map',

		module: {
			rules: [
				{
					test: /\.(tsx?)|(js)$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader',
						options: {
							cacheDirectory: true, // use cache for faster rebuilds
							babelrc: false, // don't use .babelrc if present
							presets: [
								['@babel/env', { targets: { electron: '10' } }],
								'@babel/react',
								'@babel/typescript',
							],
							plugins: [
								['@babel/plugin-proposal-class-properties', { loose: true }],
								'@babel/plugin-proposal-optional-chaining',
								isDevelopment && isHot && 'react-refresh/babel',
							].filter(Boolean),
						},
					},
				},
				{
					test: /\.css$/,
					use: [
						'style-loader',
						'css-loader',
						{
							loader: 'postcss-loader',
							options: {
								postcssOptions: {
									ident: 'postcss',
									plugins: ['tailwindcss'],
								},
							},
						},
					],
				},
			],
		},

		// eslint-disable-next-line @typescript-eslint/ban-ts-comment
		// @ts-ignore
		devServer: {
			contentBase: './dist',
			port: 9000,
			hot: true,
		},

		plugins: [
			// Used to generate the .html for electron renderer from the .html in src
			new HtmlWebpackPlugin({
				filename: 'renderer.html',
				template: './src/renderer.html',
			}),
			new CopyPlugin({
				patterns: [
					{
						from: './assets/images',
						to: 'assets/images',
					},
					{
						from: './assets/backgrounds',
						to: 'assets/backgrounds',
					},
				],
			}),
			new HardSourceWebpackPlugin(),
			isDevelopment && isHot && new ReactRefreshWebpackPlugin(),
		].filter(Boolean),
	});

	// load renderer config before to use its devServer config
	return [renderer, main];
};
