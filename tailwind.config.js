module.exports = {
	theme: {
		extend: {
			colors: {
				primary: 'hsl(356, 91%, 64%)',
				'primary-light': 'hsl(356, 91%, 70%)',
				success: 'rgb(71, 205, 98)',
				warning: 'hsl(48,  100%, 67%)', // from Bulma
				'white-overlay': 'rgba(255, 255, 255, 0.75)',
			},
			borderWidth: {
				1: '1px',
			},
			height: {
				'screen-50': '50vh',
			},
			zIndex: {
				'-1': '-1', // for background
			},
			fontSize: {
				'8xl': '6rem',
			},
		},
	},
};
