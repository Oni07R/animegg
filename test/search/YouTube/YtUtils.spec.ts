import test from 'ava';
import YtUtils from '../../../src/search/YouTube/YtUtils';

test('extractPlaylistId - extract playlist id from url', t => {
	const url =
		'https://www.youtube.com/watch?v=ULv51zuhlDk&list=PL8jk9jEnr_73zJAjpQlaj8O5JZqG5yehh';
	const id = 'PL8jk9jEnr_73zJAjpQlaj8O5JZqG5yehh';
	t.is(YtUtils.extractPlaylistId(url), id);
});

test('extractVideoId - extract video id from url', t => {
	const url =
		'https://www.youtube.com/watch?v=ULv51zuhlDk&list=PL8jk9jEnr_73zJAjpQlaj8O5JZqG5yehh';
	const id = 'ULv51zuhlDk';
	t.is(YtUtils.extractVideoId(url), id);
});
